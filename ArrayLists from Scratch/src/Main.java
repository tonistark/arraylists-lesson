import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        MyArrayList<String> array = new MyArrayList<String>();
        array.add("This");
        array.add("is");
        array.add("a");
        array.add("test.");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.add(0, "Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());
        array.remove("Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.add(1, "Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());
        array.remove("Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.add(2, "Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());
        array.remove("Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.add(3, "Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());
        array.remove("Inserted");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.add(4, "Inserted");
        System.out.println("Array Size: " + array.size());
        System.out.println(array);
        array.remove("Inserted");
        System.out.println("Array Size: " + array.size());
        System.out.println(array);

        System.out.println();

        array.remove(2);
        array.remove("Removed");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.remove(0);
        array.remove("Removed");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());

        System.out.println();

        array.clear();
        array.remove("Cleared");
        System.out.println(array);
        System.out.println("Array Size: " + array.size());
    }
}















class MyArrayList<X> {

    private int currentSize = 0;
    private X[] array;



    /** Default constructor. */
    @SuppressWarnings("unchecked")
    public MyArrayList() {
        array = (X[]) new Object[10];
    }



    /** Param constructor that starts the array off at the specified size. */
    @SuppressWarnings("unchecked")
    public MyArrayList(int initialSize) {
        if (initialSize < 0)
            throw new IllegalArgumentException();
        array = (X[]) new Object[initialSize];
    }



    /** Adds an item to the end of the ArrayList. */
    public void add(X item) {
        if (currentSize == array.length)
            doubleArraySize();
        array[currentSize] = item;
        currentSize++;
    }



    /**
     * Adds an item at the specified index. If there are elements at that index
     * and onward, they will be all be shifted up once.
     */
    public void add(int index, X item) {
        if (currentSize == array.length)
            doubleArraySize();
        if (index < 0 || index > currentSize)
            throw new IndexOutOfBoundsException();
        System.arraycopy(array, index, array, index + 1, currentSize - index);
        array[index] = item;
        currentSize++;
    }



    /** Returns the item at the specified index. */
    public X get(int index) {
        if (index < 0 || index >= currentSize)
            throw new IllegalArgumentException();
        return array[index];
    }



    /**
     * Removes the specified item, if it exists. If multiple instances exist,
     * only the first one found will be removed.
     */
    public void remove(X item) {
        for (int i = 0; i < currentSize; i++) {
            if (array[i] == item) {
                System.arraycopy(array, i + 1, array, i, currentSize - i);
                currentSize--;
                return;
            }
        }
    }



    /** Removes the item as the specified index. */
    public void remove(int index) {
        if (index < 0 || index >= currentSize)
            throw new IllegalArgumentException();
        System.arraycopy(array, index + 1, array, index, currentSize - index);
        currentSize--;
    }



    /** Checks to see if the ArrayList contains the item. */
    public boolean contains(X item) {
        for (int i = 0; i < currentSize; i++) {
            if (array[i] == item) {
                return true;
            }
        }
        return false;
    }



    /** Clears the array, resetting the size to 0. */
    public void clear() {
        for (int i = 0; i < currentSize; i++) {
            array[i] = null;
        }
        currentSize = 0;
    }



    /**
     * Returns the size of the array. Note that size refers to the number of
     * items currently held, not the total capacity.
     */
    public int size() {
        return currentSize;
    }



    /** Private helper that doubles the size of the array. */
    private void doubleArraySize() {
        array = Arrays.copyOf(array, array.length * 2);
    }



    /** Returns the internal array's total capacity. For debugging. */
    public int capacity() {
        return array.length;
    }



    /** Returns a string representation of the arraylist. */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < currentSize; i++) {
            sb.append(array[i]);
            if (i != currentSize - 1)
                sb.append(", ");
        }
        sb.append("]");
        return sb.toString();
    }
}
/*
 * [This, is, a, test.] Array Size: 4
 * 
 * [Inserted, This, is, a, test.] Array Size: 5 [This, is, a, test.] Array Size:
 * 4
 * 
 * [This, Inserted, is, a, test.] Array Size: 5 [This, is, a, test.] Array Size:
 * 4
 * 
 * [This, is, Inserted, a, test.] Array Size: 5 [This, is, a, test.] Array Size:
 * 4
 * 
 * [This, is, a, Inserted, test.] Array Size: 5 [This, is, a, test.] Array Size:
 * 4
 * 
 * Array Size: 5 [This, is, a, test., Inserted] Array Size: 4 [This, is, a,
 * test.]
 * 
 * [This, is, test.] Array Size: 3
 * 
 * [is, test.] Array Size: 2
 * 
 * [] Array Size: 0
 */